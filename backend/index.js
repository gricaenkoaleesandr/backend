import express from 'express';

const app = express();
const HOST = process.env.HOST || '127.0.0.1';
const PORT = process.env.PORT || 4444;

app.use(express.json());
app.use('/uploads', express.static('uploads')); 

app.use(configureSession());  

app.listen(PORT, HOST, (err) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log(`Server is running on http://${HOST}:${PORT}`);
});
